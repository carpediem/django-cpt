Django==1.4
MySQL-python==1.2.3
PIL==1.1.7
South==0.7.5
pycurl==7.19.0
wsgiref==0.1.2
