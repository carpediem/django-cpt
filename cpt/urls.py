# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin


admin.autodiscover()


VIEW_HOME = 'home'

VIEW_MAIN = 'main'
VIEW_WEB = 'web'



urlpatterns = patterns('',

    #url(r'^$', include('cpt.main.urls')),

    #(r'^password_reset/$', 'cpt.common.views.password_reset'),
    (r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done'),
    (r'^password_change/$', 'django.contrib.auth.views.password_change'),
    (r'^password_change/done/$', 'django.contrib.auth.views.password_change_done'),
    (r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm'),
    (r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete'),
    (r'^login/', 'django.contrib.auth.views.login', {'template_name':'auth.html'}),
    (r'^logout$', 'django.contrib.auth.views.logout', {'next_page': '/'}),

    # Admin
    url(r'^admin/', include(admin.site.urls)),



    url(r'^main/$', 'cpt.main.views.main', name=VIEW_MAIN),
    url(r'^(?P<web>.*)', 'cpt.main.views.web', name=VIEW_WEB),
)

urlpatterns += staticfiles_urlpatterns()
