# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import Group, User
from django.utils.translation import ugettext as _


class Site(models.Model):
    server = models.CharField(max_length=200,
        help_text=_("Server in lan <b>ie: 10.0.0.10</b>, (without HTTP://)"),
        verbose_name=_("server"))
    name = models.CharField(max_length=200,
        help_text=_("aplication name <b>ie: Corp Site</b>"),
        verbose_name=_("Name"))
    path = models.TextField(
        help_text=_("Aplication folders. Separatot ';' " +
            "<b>ie: /blog/;/minisite/ allways include'/'</b>"),
        verbose_name=_("Path"))
    description = models.TextField(blank=True,
        help_text=_("Application description information for users." +
        "You can include HTML"),
        verbose_name=_("Description"))
    thumbnail = models.ImageField(upload_to="thumbnails/",
        blank=True, help_text=_("94px x 75px"),
        verbose_name=_("Thumbnail"))

    def __unicode__(self):
        return self.name

    def path_urls(self):
        return self.path.split(';')

    def get_site_link(self):
        return "<a href='%s' title='%s' target='_blank' >%s</a>"  % (
            self.path.split(';')[0], self.name, self.name)

    get_site_link.allow_tags = True
    get_site_link.short_description = _("Preview")


    class Meta:
        verbose_name = _(u'Site')
        verbose_name_plural = _(u'Sites')


class GroupToSitePerm(models.Model):
    group = models.ForeignKey(Group,
        verbose_name=_("Group"))
    sites = models.ManyToManyField(Site,
        verbose_name=_("Sites"))
    description = models.TextField(blank=True,
        help_text=_("Informative description of the user group." +
            "You can include HTML"),
        verbose_name=_("Description"))

    def __unicode__(self):
        return self.group.name


    class Meta:
        verbose_name = _(u'Groups to sites permission')
        verbose_name_plural = _(u'Group to site permission')


class Configuration(models.Model):
    name = models.CharField(max_length=200,
        help_text=_("Your name"),
        verbose_name=_("Name"))
    thumbnail = models.ImageField(upload_to="thumbnails/",
        blank=True, help_text=_("94px x 75px"),
        verbose_name=_("Thumbnail"))


    def __unicode__(self):
        return self.name


    class Meta:
        verbose_name = _(u'Configuration')
        verbose_name_plural = _(u'Configurations')

