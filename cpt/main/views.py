# -*- coding: utf-8 -*-
import os
import pycurl
import re
import StringIO
from urllib import urlencode
#from urllib import urlencode,i quote

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User, Group

from cpt.main.models import GroupToSitePerm, Site


class SimpleCurl(object):

    USER_AGENT = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; EmbeddedWB 14.52 from: http://www.google.com/ EmbeddedWB 14.52; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.1; .NET CLR 1.0.3705; .NET CLR 3.0.04506.30)"

    def __init__(self):
        self.curl = pycurl.Curl()
        self.curl.setopt(pycurl.USERAGENT, SimpleCurl.USER_AGENT)
        self.cookie_filename = os.tempnam()
        self.curl.setopt(pycurl.COOKIEFILE, self.cookie_filename)
        self.curl.setopt(pycurl.FOLLOWLOCATION, True)
        self.curl.setopt(pycurl.COOKIEJAR, self.cookie_filename)

    def get_page(self, url, post_data=None):
        self.curl.setopt(pycurl.URL, str(url).replace(' ','%20'))
        if post_data:
            self.curl.setopt(pycurl.HTTPPOST, post_data)
            #~ self.curl.setopt(pycurl.POSTFIELDS, post_data)
            self.curl.setopt(pycurl.POST, 1)
        contents = StringIO.StringIO()
        self.curl.setopt(pycurl.WRITEFUNCTION, contents.write)
        self.curl.perform()
        mime = self.curl.getinfo(pycurl.CONTENT_TYPE)
        page = contents.getvalue()
        contents.close()
        return page, mime

    def __del__(self):
        self.curl.close()
        if os.path.exists(self.cookie_filename):
            os.remove(self.cookie_filename)


def web(request, web):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/auth/?next=%s" % request.path)

    user_groups = request.user.groups.all()
    # usuarios con permiso de staff pueden acceder a todos los grupos
    if request.user.is_staff:
        user_groups = Group.objects.all()

    sites = []
    for s in user_groups:
        try:
            sites.append(GroupToSitePerm.objects.get(group=s))
        except GroupToSitePerm.DoesNotExist:
            pass

    perm_sites = None
    """
    for site in sites:
        for s in site.sites.all():
            if re.match(r'^/?%s/?' % s.path, request.path): 
                perm_sites = s
                break
    """
    for site in sites:
        for site_urls in site.sites.all():
            for s in site_urls.path_urls():
                if re.match(r'^/?%s/?' % s, request.path): 
                    perm_sites = s
                    break


    if not perm_sites:
        return HttpResponseRedirect("/main")
    mycurl = SimpleCurl()
    post_data = None
    if request.method=="POST":
        post_data = request.raw_post_data
    page = mycurl.get_page("http://%s/%s" % (site_urls.server, web), post_data)
    return HttpResponse(page[0], mimetype=page[1])


def main(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/auth/?next=%s" % request.path)
    user_groups = request.user.groups.all()
    sites = []
    for s in user_groups:
        try:
            sites.append(GroupToSitePerm.objects.get(group=s))
        except GroupToSitePerm.DoesNotExist:
            pass
    return render_to_response('main.html', RequestContext(request, {'sites':sites, 'user_groups':user_groups, 
    'path':request.path[1:], 'is_staff':request.user.is_staff }))


def password_reset(request):
    """
    django.contrib.auth.views.password_reset view (forgotten password)
    """
    if not request.user.is_authenticated():
        from django.contrib.auth.views import password_reset
        return password_reset(request, post_reset_redirect='/password_reset/done/')
    else:
        return HttpResponseRedirect("/main/")
