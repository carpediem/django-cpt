# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns, url


VIEW_MAIN = 'main'
VIEW_WEB = 'web'


urlpatterns = patterns('',

    url(r'^$', 'cpt.main.views.main', name=VIEW_MAIN),
    url(r'^(?P<web>.*)', 'cpt.main.views.web', name=VIEW_WEB),
)
