# -*- coding: utf-8 -*-

from django.contrib import admin

from cpt.main.models import GroupToSitePerm, Site, Configuration


class SiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'server', 'path', 'get_site_link')


    class Media:
        js = ('/static/js/jquery.js',
                '/static/js/tiny_mce/jquery.tinymce.js',
                '/static/js/textareas.js',)


class GroupToSitePermAdmin(admin.ModelAdmin):


    class Media:
        js = ('/static/js/jquery.js',
                '/static/js/tiny_mce/jquery.tinymce.js',
                '/static/js/textareas.js',)


class ConfigurationAdmin(admin.ModelAdmin):
    list_display = ('name',)

admin.site.register(Site, SiteAdmin)
admin.site.register(GroupToSitePerm, GroupToSitePermAdmin)
admin.site.register(Configuration, ConfigurationAdmin)
